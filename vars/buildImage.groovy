#!/user/bin/env groovy


def call(
    echo "building the docker image"
    echo "building version"  
    withCredentials([usernamePassword(credentialsId: 'dockerhub-repo', usernameVariable: 'USER', passwordVariable: 'PASS')]){
        sh "docker build -t georgeebeh/jma:1.2 ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push georgeebeh/jma:1.2"
    }
)